package ibb.api.phenotypeservice.search.queryparser;

public class QueryParserException extends Exception {
    public QueryParserException(String message) {
        super(message);
    }
}
