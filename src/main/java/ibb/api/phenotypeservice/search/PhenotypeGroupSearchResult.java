package ibb.api.phenotypeservice.search;

import java.util.List;

import ibb.api.phenotypeservice.core.PhenotypeGroup;

public class PhenotypeGroupSearchResult {
    public long total;
    public List<PhenotypeGroup> data;
}
